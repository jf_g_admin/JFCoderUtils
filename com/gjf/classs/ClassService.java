package gjf.classs;

/**
 * @author JianFeiGan
 * @date 2022/5/20
 * @description 类扩展接口
 */
public interface ClassService {

    /**
     * 重新加载bean到JVM
     * @param targetUrl class/jar 本地文件地址
     * @param classUrl 类路径
     * @param sp 是否加载到spring容器
     * */
    public void reload(String targetUrl,String classUrl,boolean sp);

}

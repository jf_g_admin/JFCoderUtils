package gjf.classs.impl;

import gjf.classs.ClassService;
import gjf.classs.utils.*;
import gjf.classs.utils.SpringUtil;

import javax.annotation.Resource;

/**
 * @author JianFeiGan
 * @date 2022/5/20
 * @description 类加载实现类
 */
public class ClassServiceImpl implements ClassService {

    @Resource
    private SpringUtil springUtil;

    /**
     * 重新加载bean到JVM
     *
     * @param targetUrl class/jar 本地文件地址
     * @param classUrl  类路径
     * @param sanc 是否加载到spring容器
     */
    @Override
    public void reload(String targetUrl, String classUrl,boolean sanc) {
        ClassLoader classLoader = ClassLoaderUtil.getClassLoader(targetUrl);
        try {
            Class<?> clazz = classLoader.loadClass(classUrl);
            if (sanc) {
                springUtil.registerBean(clazz.getName(), clazz);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


}
